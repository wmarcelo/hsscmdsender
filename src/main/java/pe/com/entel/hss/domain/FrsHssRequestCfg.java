package pe.com.entel.hss.domain;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Date;

/**
 * @version 1.0, 09/01/2018
 * @autor jsegovia
 */
public class FrsHssRequestCfg implements Serializable, SQLData {

    private String sql_type;

    private Integer numHssRequestCfgId;

    private Integer numHssStatus;

    private String vchHssDatatype;

    private String vchHssKey;

    private String vchHssValue;

    private String vchHssValue2;

    private String vchHssDescription;

    private Date fecHssRegdate;

    private Date fecHssUpddate;


    public Integer getNumHssRequestCfgId() {
        return numHssRequestCfgId;
    }

    public void setNumHssRequestCfgId(Integer numHssRequestCfgId) {
        this.numHssRequestCfgId = numHssRequestCfgId;
    }

    public Integer getNumHssStatus() {
        return numHssStatus;
    }

    public void setNumHssStatus(Integer numHssStatus) {
        this.numHssStatus = numHssStatus;
    }

    public String getVchHssDatatype() {
        return vchHssDatatype;
    }

    public void setVchHssDatatype(String vchHssDatatype) {
        this.vchHssDatatype = vchHssDatatype;
    }

    public String getVchHssKey() {
        return vchHssKey;
    }

    public void setVchHssKey(String vchHssKey) {
        this.vchHssKey = vchHssKey;
    }

    public String getVchHssValue() {
        return vchHssValue;
    }

    public void setVchHssValue(String vchHssValue) {
        this.vchHssValue = vchHssValue;
    }

    public String getVchHssValue2() {
        return vchHssValue2;
    }

    public void setVchHssValue2(String vchHssValue2) {
        this.vchHssValue2 = vchHssValue2;
    }

    public String getVchHssDescription() {
        return vchHssDescription;
    }

    public void setVchHssDescription(String vchHssDescription) {
        this.vchHssDescription = vchHssDescription;
    }

    public Date getFecHssRegdate() {
        return fecHssRegdate;
    }

    public void setFecHssRegdate(Date fecHssRegdate) {
        this.fecHssRegdate = fecHssRegdate;
    }

    public Date getFecHssUpddate() {
        return fecHssUpddate;
    }

    public void setFecHssUpddate(Date fecHssUpddate) {
        this.fecHssUpddate = fecHssUpddate;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return "EIR.TO_FRS_HSS_REQUEST_CFG";
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        sql_type = typeName;

        setNumHssRequestCfgId(stream.readInt());
        setNumHssStatus(stream.readInt());
        setVchHssDatatype(stream.readString());
        setVchHssKey(stream.readString());
        setVchHssValue(stream.readString());
        setVchHssValue2(stream.readString());
        setVchHssDescription(stream.readString());

        if (stream.readDate() != null) {
            setFecHssRegdate(new Date(stream.readDate().getTime()));
        }

        if (stream.readDate() != null) {
            setFecHssUpddate(new Date(stream.readDate().getTime()));
        }
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(getNumHssRequestCfgId());
        stream.writeInt(getNumHssStatus());
        stream.writeString(getVchHssDatatype());
        stream.writeString(getVchHssKey());
        stream.writeString(getVchHssValue());
        stream.writeString(getVchHssValue2());
        stream.writeString(getVchHssDescription());

        if (getFecHssRegdate() != null) {
            stream.writeDate(new java.sql.Date(getFecHssRegdate().getTime()));
        } else {
            stream.writeDate(null);
        }

        if (getFecHssUpddate() != null) {
            stream.writeDate(new java.sql.Date(getFecHssUpddate().getTime()));
        } else {
            stream.writeDate(null);
        }
    }
}
