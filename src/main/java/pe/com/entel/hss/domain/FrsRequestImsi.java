package pe.com.entel.hss.domain;

import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

public class FrsRequestImsi implements Serializable, SQLData {

    private String hostNB;
    private int portNB;
    private int timeoutReadNB;
    private int timeoutConnectNB;
    private String login1CmdNB;
    private String login2CmdNB;
    private String login3CmdNB;
    private String loginValNB;
    private String logout1CmdNB;
    private String logout2CmdNB;
    private String logout3CmdNB;
    private String logoutValNB;
    private String regneCmdNB;
    private String regneValNB;
    private int maxConnNB;
    private String dmsWsUrl;

    public String getHostNB() {
        return hostNB;
    }

    public void setHostNB(String hostNB) {
        this.hostNB = hostNB;
    }

    public int getPortNB() {
        return portNB;
    }

    public void setPortNB(int portNB) {
        this.portNB = portNB;
    }

    public int getTimeoutReadNB() {
        return timeoutReadNB;
    }

    public void setTimeoutReadNB(int timeoutReadNB) {
        this.timeoutReadNB = timeoutReadNB;
    }

    public int getTimeoutConnectNB() {
        return timeoutConnectNB;
    }

    public void setTimeoutConnectNB(int timeoutConnectNB) {
        this.timeoutConnectNB = timeoutConnectNB;
    }

    public String getLogin1CmdNB() {
        return login1CmdNB;
    }

    public void setLogin1CmdNB(String login1CmdNB) {
        this.login1CmdNB = login1CmdNB;
    }

    public String getLogin2CmdNB() {
        return login2CmdNB;
    }

    public void setLogin2CmdNB(String login2CmdNB) {
        this.login2CmdNB = login2CmdNB;
    }

    public String getLogin3CmdNB() {
        return login3CmdNB;
    }

    public void setLogin3CmdNB(String login3CmdNB) {
        this.login3CmdNB = login3CmdNB;
    }

    public String getLoginValNB() {
        return loginValNB;
    }

    public void setLoginValNB(String loginValNB) {
        this.loginValNB = loginValNB;
    }

    public String getLogout1CmdNB() {
        return logout1CmdNB;
    }

    public void setLogout1CmdNB(String logout1CmdNB) {
        this.logout1CmdNB = logout1CmdNB;
    }

    public String getLogout2CmdNB() {
        return logout2CmdNB;
    }

    public void setLogout2CmdNB(String logout2CmdNB) {
        this.logout2CmdNB = logout2CmdNB;
    }

    public String getLogout3CmdNB() {
        return logout3CmdNB;
    }

    public void setLogout3CmdNB(String logout3CmdNB) {
        this.logout3CmdNB = logout3CmdNB;
    }

    public String getLogoutValNB() {
        return logoutValNB;
    }

    public void setLogoutValNB(String logoutValNB) {
        this.logoutValNB = logoutValNB;
    }

    public int getMaxConnNB() {
        return maxConnNB;
    }

    public void setMaxConnNB(int maxConnNB) {
        this.maxConnNB = maxConnNB;
    }

    public String getDmsWsUrl() {
        return dmsWsUrl;
    }

    public void setDmsWsUrl(String dmsWsUrl) {
        this.dmsWsUrl = dmsWsUrl;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return null;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {

    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {

    }

    public String getRegneCmdNB() {
        return regneCmdNB;
    }

    public void setRegneCmdNB(String regneCmdNB) {
        this.regneCmdNB = regneCmdNB;
    }

    public String getRegneValNB() {
        return regneValNB;
    }

    public void setRegneValNB(String regneValNB) {
        this.regneValNB = regneValNB;
    }

}
