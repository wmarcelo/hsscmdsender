package pe.com.entel.hss.mapper;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import pe.com.entel.hss.domain.FrsHssRequestCfg;
import pe.com.entel.hss.util.DateUtil;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @version 1.0, 06/01/2018
 * @autor jsegovia
 */
public class FrsHssRequestCfgMapper implements RowMapper<FrsHssRequestCfg> {

    private Logger logger = Logger.getLogger(FrsHssRequestCfgMapper.class);

    @Override
    public FrsHssRequestCfg mapRow(ResultSet rs, int i) throws SQLException {
        logger.debug("apply rowmapper FrsHssRequestCfg");

        FrsHssRequestCfg o = new FrsHssRequestCfg();
        o.setNumHssRequestCfgId(rs.getInt("NUM_HSS_REQUEST_CFG_ID"));
        o.setNumHssStatus(rs.getInt("NUM_HSS_STATUS"));
        o.setVchHssDatatype(rs.getString("VCH_HSS_DATATYPE"));
        o.setVchHssKey(rs.getString("VCH_HSS_KEY"));
        o.setVchHssValue(rs.getString("VCH_HSS_VALUE"));
        o.setVchHssValue2(rs.getString("VCH_HSS_VALUE2"));
        o.setVchHssDescription(rs.getString("VCH_HSS_DESCRIPTION"));
        o.setFecHssRegdate(DateUtil.convertDate(rs.getDate("FEC_HSS_REGDATE")));
        o.setFecHssUpddate(DateUtil.convertDate(rs.getDate("FEC_HSS_UPDDATE")));

        return o;
    }
}
