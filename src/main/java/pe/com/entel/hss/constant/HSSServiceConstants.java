package pe.com.entel.hss.constant;

/**
 * @version 1.0, 22/01/2018
 * @autor jsegovia
 */
public class HSSServiceConstants {

    private HSSServiceConstants() {
    }

    public final static String RESPONSE_WS_MAIL_OK = "0";

    public static final String MAX_REQ_CMDS = "MAX_REQ_CMDS";

    public static final String HSS_HOST = "HSS_HOST";

    public static final String HSS_PORT = "HSS_PORT";

    public static final String HSS_TIMEOUTCONNECT = "HSS_TIMEOUTCONNECT";

    public static final String HSS_TIMEOUTREAD = "HSS_TIMEOUTREAD";

    public static final String HSS_LOGIN_CMD = "HSS_LOGIN_CMD";

    public static final String HSS_LOGIN_VAL = "HSS_LOGIN_VAL";

    public static final String HSS_LOGIN_ERR = "HSS_LOGIN_ERR";

    public static final String HSS_LOGOUT_CMD = "HSS_LOGOUT_CMD";

    public static final String HSS_LOGOUT_VAL = "HSS_LOGOUT_VAL";

    public static final String HSS_LOGOUT_ERR = "HSS_LOGOUT_ERR";

    public static final String HSS_CANCELC_CMD = "HSS_CANCELC_CMD";

    public static final String HSS_CANCELC_VAL = "HSS_CANCELC_VAL";

    public static final String HSS_CANCELC_ERR = "HSS_CANCELC_ERR";

    public static final String HSS_END_CMD = "HSS_END_CMD";

    public static final String HSS_MAX_RETRY = "HSS_MAX_RETRY";

    public static final String HSS_MAX_CONN_HSS = "HSS_MAX_CONN_HSS";

    public static final String HSS_DELEY_REPROC = "HSS_DELEY_REPROC";

    public static final String HSS_REQUEST_FRECUENCY = "HSS_REQUEST_FRECUENCY";

    public static final String MAIL_FRECUENCY = "MAIL_FRECUENCY";

    public static final String MAIL_FLAG = "MAIL_FLAG";

    public static final String MAIL_SERVICE_URL = "MAIL_SERVICE_URL";

    public static final String MAIL_FROM = "MAIL_FROM";

    public static final String MAIL_TO = "MAIL_TO";

    public static final String MAIL_SUBJECT = "MAIL_SUBJECT";

    public static final String MAIL_MESSAGE = "MAIL_MESSAGE";

    public static final String MVHIST_FRECUENCY = "MVHIST_FRECUENCY";

    public static final String NB_HOST = "NB_HOST";

    public static final String NB_PORT = "NB_PORT";

    public static final String DMS_WS_URL = "DMS_WS_URL";

    public static final String NB_LOGIN1_CMD = "NB_LOGIN1_CMD";

    public static final String NB_LOGIN_VAL = "NB_LOGIN_VAL";

    public static final String NB_LOGOUT1_CMD = "NB_LOGOUT1_CMD";

    public static final String NB_LOGOUT_VAL = "NB_LOGOUT_VAL";

    public static final String NB_LOGIN2_CMD = "NB_LOGIN2_CMD";

    public static final String NB_LOGOUT2_CMD = "NB_LOGOUT2_CMD";

    public static final String NB_LOGIN3_CMD = "NB_LOGIN3_CMD";

    public static final String NB_LOGOUT3_CMD = "NB_LOGOUT3_CMD";

    public static final String NB_REGNE_CMD = "NB_REGNE_CMD";

    public static final String NB_REGNE_VAL = "NB_REGNE_VAL";

    public static final String NB_TIMEOUTCONNECT = "NB_TIMEOUTCONNECT";

    public static final String NB_TIMEOUTREAD = "NB_TIMEOUTREAD";

    public static final int IMSI_FINAL_POSITION = 20;

}
