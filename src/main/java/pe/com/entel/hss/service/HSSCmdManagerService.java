package pe.com.entel.hss.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import pe.com.entel.hss.domain.FrsHssRequest;
import pe.com.entel.hss.domain.FrsRequestImsi;
import pe.com.entel.hss.exception.RepositoryException;
import pe.com.entel.hss.exception.ValueNotFoundException;
import pe.com.entel.hss.repository.FrsHssRequestRepository;
import pe.com.entel.hss.task.SendCmdHssTask;
import pe.com.entel.hss.util.TelnetClientHandler;
import pe.com.entel.hss.util.TelnetClientHandlerPool;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import static pe.com.entel.hss.constant.HSSServiceConstants.*;

/**
 * @version 1.0, 06/01/2018
 * @autor jsegovia
 */
@Service
public class HSSCmdManagerService {

    private static Logger logger = Logger.getLogger(HSSCmdManagerService.class);

    @Autowired
    private HSSInitialConfig hSSInitialConfig;

    @Autowired
    private FrsHssRequestRepository frsHssRequestRepository;

    @Autowired
    private ThreadPoolTaskExecutor cmdHssTaskExecutor;

    @Autowired
    private ApplicationContext applicationContext;

    public void sendParalelCmds() {
        try {
            logger.info(">>>> Iniciando Servicio envio de comandos ... <<<<");

            Integer maxReg = hSSInitialConfig.getIntParam(MAX_REQ_CMDS);
            verifyValue(maxReg, MAX_REQ_CMDS);

            String hss_host = hSSInitialConfig.getStrParam(HSS_HOST);
            verifyValue(hss_host, HSS_HOST);

            Integer hss_port = hSSInitialConfig.getIntParam(HSS_PORT);
            verifyValue(hss_port, HSS_PORT);

            Integer hss_timeoutconnect = hSSInitialConfig.getIntParam(HSS_TIMEOUTCONNECT);
            verifyValue(hss_timeoutconnect, HSS_TIMEOUTCONNECT);

            Integer hss_timeoutread = hSSInitialConfig.getIntParam(HSS_TIMEOUTREAD);
            verifyValue(hss_timeoutread, HSS_TIMEOUTREAD);

            String hss_login_cmd = hSSInitialConfig.getStrParam(HSS_LOGIN_CMD);
            verifyValue(hss_login_cmd, HSS_LOGIN_CMD);

            String hss_login_val = hSSInitialConfig.getStrParam(HSS_LOGIN_VAL);
            verifyValue(hss_login_val, HSS_LOGIN_VAL);

            String hss_login_err = hSSInitialConfig.getStrParam(HSS_LOGIN_ERR);
            verifyValue(hss_login_err, HSS_LOGIN_ERR);

            String hss_logout_cmd = hSSInitialConfig.getStrParam(HSS_LOGOUT_CMD);
            verifyValue(hss_logout_cmd, HSS_LOGOUT_CMD);

            String hss_logout_val = hSSInitialConfig.getStrParam(HSS_LOGOUT_VAL);
            verifyValue(hss_logout_val, HSS_LOGOUT_VAL);

            String hss_logout_err = hSSInitialConfig.getStrParam(HSS_LOGOUT_ERR);
            verifyValue(hss_logout_err, HSS_LOGOUT_ERR);

            String hss_cancelc_cmd = hSSInitialConfig.getStrParam(HSS_CANCELC_CMD);
            verifyValue(hss_cancelc_cmd, HSS_CANCELC_CMD);

            String hss_cancelc_val = hSSInitialConfig.getStrParam(HSS_CANCELC_VAL);
            verifyValue(hss_cancelc_val, HSS_CANCELC_VAL);

            String hss_cancelc_err = hSSInitialConfig.getStrParam(HSS_CANCELC_ERR);
            verifyValue(hss_cancelc_err, HSS_CANCELC_ERR);

            String hss_cmd_end = hSSInitialConfig.getStrParam(HSS_END_CMD);
            verifyValue(hss_cancelc_err, HSS_END_CMD);

            Integer hss_max_retry = hSSInitialConfig.getIntParam(HSS_MAX_RETRY);
            verifyValue(hss_cancelc_err, HSS_MAX_RETRY);

            Integer hss_max_conn_hss = hSSInitialConfig.getIntParam(HSS_MAX_CONN_HSS);
            verifyValue(hss_max_conn_hss, HSS_MAX_CONN_HSS);

            Integer hss_delay_reproc = hSSInitialConfig.getIntParam((HSS_DELEY_REPROC));
            verifyValue(hss_max_conn_hss, HSS_DELEY_REPROC);

            String dms_ws_url = hSSInitialConfig.getStrParam((DMS_WS_URL));
            verifyValue(dms_ws_url, DMS_WS_URL);

            //Variables usadas para el request del imsi
            String NB_host = hSSInitialConfig.getStrParam(NB_HOST);
            verifyValue(NB_host, NB_HOST);

            Integer NB_port = hSSInitialConfig.getIntParam(NB_PORT);
            verifyValue(NB_port, NB_PORT);

            String nb_login1_cmd = hSSInitialConfig.getStrParam(NB_LOGIN1_CMD);
            verifyValue(nb_login1_cmd, NB_LOGIN1_CMD);

            String nb_login1_val = hSSInitialConfig.getStrParam(NB_LOGIN_VAL);
            verifyValue(nb_login1_val, NB_LOGIN_VAL);

            String nb_logout1_cmd = hSSInitialConfig.getStrParam(NB_LOGOUT1_CMD);
            verifyValue(nb_logout1_cmd, NB_LOGOUT1_CMD);

            String nb_logout1_val = hSSInitialConfig.getStrParam(NB_LOGOUT_VAL);
            verifyValue(nb_logout1_val, NB_LOGOUT_VAL);


            String nb_login2_cmd = hSSInitialConfig.getStrParam(NB_LOGIN2_CMD);
            verifyValue(nb_login2_cmd, NB_LOGIN2_CMD);

            String nb_logout2_cmd = hSSInitialConfig.getStrParam(NB_LOGOUT2_CMD);
            verifyValue(nb_logout2_cmd, NB_LOGOUT2_CMD);

            String nb_login3_cmd = hSSInitialConfig.getStrParam(NB_LOGIN3_CMD);
            verifyValue(nb_login3_cmd, NB_LOGIN3_CMD);

            String nb_logout3_cmd = hSSInitialConfig.getStrParam(NB_LOGOUT3_CMD);
            verifyValue(nb_logout3_cmd, NB_LOGOUT3_CMD);

            String nb_regne_cmd = hSSInitialConfig.getStrParam(NB_REGNE_CMD);
            verifyValue(nb_regne_cmd, NB_REGNE_CMD);

            String nb_regne_val = hSSInitialConfig.getStrParam(NB_REGNE_VAL);
            verifyValue(nb_regne_val, NB_REGNE_VAL);

            int nb_timeout_connect = hSSInitialConfig.getIntParam(NB_TIMEOUTCONNECT);
            verifyValue(nb_timeout_connect, NB_TIMEOUTCONNECT);

            int nb_timeout_read = hSSInitialConfig.getIntParam(NB_TIMEOUTREAD);
            verifyValue(nb_timeout_read, NB_TIMEOUTREAD);

            FrsRequestImsi frsRequestImsi = new FrsRequestImsi();
            frsRequestImsi.setHostNB(NB_host);
            frsRequestImsi.setPortNB(NB_port);

            frsRequestImsi.setLogin1CmdNB(nb_login1_cmd);
            frsRequestImsi.setLogout1CmdNB(nb_logout1_cmd);
            frsRequestImsi.setLogin2CmdNB(nb_login2_cmd);
            frsRequestImsi.setLogout2CmdNB(nb_logout2_cmd);
            frsRequestImsi.setLogin3CmdNB(nb_login3_cmd);
            frsRequestImsi.setLogout3CmdNB(nb_logout3_cmd);
            frsRequestImsi.setLogoutValNB(nb_logout1_val);
            frsRequestImsi.setLoginValNB(nb_login1_val);
            frsRequestImsi.setRegneCmdNB(nb_regne_cmd);
            frsRequestImsi.setRegneValNB(nb_regne_val);
            frsRequestImsi.setTimeoutConnectNB(nb_timeout_connect);
            frsRequestImsi.setTimeoutReadNB(nb_timeout_read);

            frsRequestImsi.setDmsWsUrl(dms_ws_url);

            TelnetClientHandler.TelnetClientHandlerBuilder paramsEirTelnetBuilder
                    = new TelnetClientHandler.TelnetClientHandlerBuilder();
            paramsEirTelnetBuilder.host(hss_host);
            paramsEirTelnetBuilder.port(hss_port);
            paramsEirTelnetBuilder.timeoutRead(hss_timeoutread);
            paramsEirTelnetBuilder.timeoutConnect(hss_timeoutconnect);
            paramsEirTelnetBuilder.loginCmd(hss_login_cmd);
            paramsEirTelnetBuilder.loginVal(hss_login_val);
            paramsEirTelnetBuilder.loginErr(hss_login_err);
            paramsEirTelnetBuilder.logoutCmd(hss_logout_cmd);
            paramsEirTelnetBuilder.logoutVal(hss_logout_val);
            paramsEirTelnetBuilder.logoutErr(hss_logout_err);

            logger.info("Listando peticiones en estado RETRY a procesar ...");

            List<FrsHssRequest> requestsErr = frsHssRequestRepository.getByMaxRegAndStatus(maxReg,
                    FrsHssRequest.STATUS_RETRY);

            if (requestsErr.isEmpty()) {
                logger.warn("No se encontraron peticiones en estado RETRY!");
            } else {
                CountDownLatch cdlErr = new CountDownLatch(requestsErr.size());

                Date curr_date = new Date();
                logger.debug("Fecha actual : " + curr_date);

                for (FrsHssRequest rErr : requestsErr) {
                    logger.debug("Fecha del registro : " + rErr.getFecHssRequestDeferreddate());
                    if (rErr.getFecHssRequestDeferreddate() != null && rErr.getFecHssRequestDeferreddate().compareTo(curr_date) == 1) {
                        cdlErr.countDown();
                        logger.debug("No se procesara este registro n. : " + rErr.getNumHssRequestId());
                        continue;
                    }

                    SendCmdHssTask task = applicationContext.getBean(SendCmdHssTask.class);

                    task.setTelnetEIRClientHandlerBuilder(paramsEirTelnetBuilder);
                    task.setFrsHssRequest(rErr);
                    task.setCommandFormat(hss_cancelc_cmd);
                    task.setCommandResSucc(hss_cancelc_val);
                    task.setCommandResErr(hss_cancelc_err);
                    task.setCmdEnd(hss_cmd_end);
                    task.setMaxRetry(hss_max_retry);
                    task.setTimeToReproc(hss_delay_reproc);
                    task.setCountDownLatch(cdlErr);
                    task.setFrsRequestImsi(frsRequestImsi);
                    cmdHssTaskExecutor.execute(task);
                }

                cdlErr.await();

                logger.warn("Fin procesamiento de peticiones en estado RETRY");
            }

            logger.info("Listando peticiones en estado REGISTRADOS a procesar ...");

            List<FrsHssRequest> requests = frsHssRequestRepository.getByMaxRegAndStatus(maxReg,
                    FrsHssRequest.STATUS_REGISTERED);

            if (requests.isEmpty()) {
                logger.warn("No se encontraron peticiones en estado REGISTRADO");
                return;
            }

            logger.info("Procensando [ " + requests.size() + " ] registros ...");

            CountDownLatch cdl = new CountDownLatch(requests.size());

            for (FrsHssRequest r : requests) {
                SendCmdHssTask task = applicationContext.getBean(SendCmdHssTask.class);

                task.setTelnetEIRClientHandlerBuilder(paramsEirTelnetBuilder);
                task.setFrsHssRequest(r);
                task.setCommandFormat(hss_cancelc_cmd);
                task.setCommandResSucc(hss_cancelc_val);
                task.setCommandResErr(hss_cancelc_err);
                task.setCmdEnd(hss_cmd_end);
                task.setMaxRetry(hss_max_retry);
                task.setTimeToReproc(hss_delay_reproc);
                task.setCountDownLatch(cdl);
                task.setFrsRequestImsi(frsRequestImsi);
                cmdHssTaskExecutor.execute(task);
            }

            cdl.await();

            logger.info("Fin procesamiento de peticiones en estado REGISTRADOS");

        } catch (RepositoryException e) {
            logger.error(e);
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        } catch (ValueNotFoundException e) {
            logger.error(e.getMessage());
        } finally {
            logger.info(">>>> Fin Iniciando Servicio envio de comandos ... <<<<");
        }
    }

    public void verifyValue(Object object, String varName) throws ValueNotFoundException {
        if (object == null) {
            throw new ValueNotFoundException(String.format("La variable %s no ha sido definido en la base de datos", varName));
        }
    }

}
