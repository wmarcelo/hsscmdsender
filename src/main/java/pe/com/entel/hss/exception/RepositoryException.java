package pe.com.entel.hss.exception;

/**
 * @version 1.0, 06/01/2018
 * @autor jsegovia
 */
public class RepositoryException extends Exception {

    public RepositoryException(Exception ex) {
        super(ex);
    }

    public RepositoryException(String message) {
        super(message);
    }

}
