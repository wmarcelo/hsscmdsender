package pe.com.entel.hss.exception;

/**
 * @version 1.0, 22/01/2018
 * @autor jsegovia
 */
public class ImsiNotFoundException extends Exception {

    public ImsiNotFoundException(Exception ex) {
        super(ex);
    }

    public ImsiNotFoundException(String message) {
        super(message);
    }

}
