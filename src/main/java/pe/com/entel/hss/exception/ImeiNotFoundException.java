package pe.com.entel.hss.exception;

/**
 * @version 1.0, 22/01/2018
 * @autor jsegovia
 */
public class ImeiNotFoundException extends Exception {

    public ImeiNotFoundException(Exception ex) {
        super(ex);
    }

    public ImeiNotFoundException(String message) {
        super(message);
    }

}
