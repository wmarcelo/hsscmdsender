package pe.com.entel.hss.exception;

/**
 * @version 1.0, 22/01/2018
 * @autor jsegovia
 */
public class ValueNotFoundException extends Exception {

    public ValueNotFoundException(Exception ex) {
        super(ex);
    }

    public ValueNotFoundException(String message) {
        super(message);
    }

}
