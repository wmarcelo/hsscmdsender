package pe.com.entel.hss.util;

import java.util.Date;

/**
 * @version 1.0, 08/01/2018
 * @autor jsegovia
 */
public class DateUtil {

    public static Date convertDate(java.sql.Date dateSql) {
        if (dateSql == null) {
            return null;
        }
        return new Date(dateSql.getTime());
    }

    public static Date convertDate(java.sql.Timestamp timestamp) {
        if (timestamp == null) {
            return null;
        }
        return new Date(timestamp.getTime());
    }


}