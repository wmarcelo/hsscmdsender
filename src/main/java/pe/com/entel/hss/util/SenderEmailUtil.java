package pe.com.entel.hss.util;

import org.apache.log4j.Logger;
import pe.com.entel.hss.ws.*;

import java.net.URL;
import java.util.List;

public class SenderEmailUtil {

    static Logger logger = Logger.getLogger(SenderEmailUtil.class);

    public static EnviarEmailResponse send(String serviceUrl, String from, List<String> to, String subject, String message) throws Exception {

        EnviarEmailRequest request = new EnviarEmailRequest();
        NotificacionType notificacionType = new NotificacionType();

        //Importancia
        notificacionType.setImportancia(ImportanciaType.NORMAL);

        // From
        notificacionType.setOrigen(from);

        // To
        NotificacionType.ListaDestinos destinosTo = new NotificacionType.ListaDestinos();
        destinosTo.getDestino().addAll(to);
        notificacionType.setListaDestinos(destinosTo);

        //Subject
        notificacionType.setAsunto(subject);

        // Texto
        notificacionType.setTexto(message);

        // Archivos adjuntos
        NotificacionType.ListaAdjunto listaAdjunto = new NotificacionType.ListaAdjunto();

        notificacionType.setListaAdjunto(listaAdjunto);

        request.setNotificarRequestType(notificacionType);

        NotificarService service = new NotificarService(new URL(serviceUrl));

        return service.getNotificarPort().enviarEmail(request);
    }
}
