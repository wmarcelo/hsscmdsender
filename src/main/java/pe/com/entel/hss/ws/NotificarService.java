
package pe.com.entel.hss.ws;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.10
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "NotificarService", targetNamespace = "http://entel.com.pe/integracion/notificar/ws", wsdlLocation = "http://172.28.201.123:7003/Notificaciones/notificarPort?wsdl")
public class NotificarService
    extends Service
{

    private final static URL NOTIFICARSERVICE_WSDL_LOCATION;
    private final static WebServiceException NOTIFICARSERVICE_EXCEPTION;
    private final static QName NOTIFICARSERVICE_QNAME = new QName("http://entel.com.pe/integracion/notificar/ws", "NotificarService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://172.28.201.123:7003/Notificaciones/notificarPort?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        NOTIFICARSERVICE_WSDL_LOCATION = url;
        NOTIFICARSERVICE_EXCEPTION = e;
    }

    public NotificarService() {
        super(__getWsdlLocation(), NOTIFICARSERVICE_QNAME);
    }

    public NotificarService(WebServiceFeature... features) {
        super(__getWsdlLocation(), NOTIFICARSERVICE_QNAME, features);
    }

    public NotificarService(URL wsdlLocation) {
        super(wsdlLocation, NOTIFICARSERVICE_QNAME);
    }

    public NotificarService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, NOTIFICARSERVICE_QNAME, features);
    }

    public NotificarService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public NotificarService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns NotificarPT
     */
    @WebEndpoint(name = "notificarPort")
    public NotificarPT getNotificarPort() {
        return super.getPort(new QName("http://entel.com.pe/integracion/notificar/ws", "notificarPort"), NotificarPT.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns NotificarPT
     */
    @WebEndpoint(name = "notificarPort")
    public NotificarPT getNotificarPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://entel.com.pe/integracion/notificar/ws", "notificarPort"), NotificarPT.class, features);
    }

    private static URL __getWsdlLocation() {
        if (NOTIFICARSERVICE_EXCEPTION!= null) {
            throw NOTIFICARSERVICE_EXCEPTION;
        }
        return NOTIFICARSERVICE_WSDL_LOCATION;
    }

}
