
package pe.com.entel.hss.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ImportanciaType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ImportanciaType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NORMAL"/>
 *     &lt;enumeration value="ALTA"/>
 *     &lt;enumeration value="BAJA"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ImportanciaType", namespace = "http://entel.com.pe/integracion/notificar/schema")
@XmlEnum
public enum ImportanciaType {

    NORMAL,
    ALTA,
    BAJA;

    public String value() {
        return name();
    }

    public static ImportanciaType fromValue(String v) {
        return valueOf(v);
    }

}
