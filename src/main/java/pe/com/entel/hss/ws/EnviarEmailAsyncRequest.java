
package pe.com.entel.hss.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="notificarRequestType" type="{http://entel.com.pe/integracion/notificar/schema}NotificacionType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "notificarRequestType"
})
@XmlRootElement(name = "enviarEmailAsyncRequest")
public class EnviarEmailAsyncRequest {

    protected NotificacionType notificarRequestType;

    /**
     * Gets the value of the notificarRequestType property.
     * 
     * @return
     *     possible object is
     *     {@link NotificacionType }
     *     
     */
    public NotificacionType getNotificarRequestType() {
        return notificarRequestType;
    }

    /**
     * Sets the value of the notificarRequestType property.
     * 
     * @param value
     *     allowed object is
     *     {@link NotificacionType }
     *     
     */
    public void setNotificarRequestType(NotificacionType value) {
        this.notificarRequestType = value;
    }

}
