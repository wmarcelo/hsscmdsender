package pe.com.entel.hss.repository;

import oracle.jdbc.OracleTypes;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import pe.com.entel.hss.domain.FrsHssRequestCfg;
import pe.com.entel.hss.exception.RepositoryException;
import pe.com.entel.hss.mapper.FrsHssRequestCfgMapper;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0, 06/01/2018
 * @autor jsegovia
 */
@Repository
public class FrsHssRequestCfgRepository {

    private Logger logger = Logger.getLogger(FrsHssRequestCfgRepository.class);

    @Autowired
    @Qualifier("hssDataSource")
    private DataSource ds;

    @Value("${hss.bd.schema}")
    private String schema;

    @Value("${hss.bd.package}")
    private String mainPackage;

    public List<FrsHssRequestCfg> getAll() throws RepositoryException {

        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(ds);
            jdbcCall.withSchemaName(schema);
            jdbcCall.withCatalogName(mainPackage);
            jdbcCall.withProcedureName("SP_RS_CD_GETALLCONFIG");

            jdbcCall.addDeclaredParameter(new SqlOutParameter("ACUR_CONFIG_LIST", OracleTypes.CURSOR, new FrsHssRequestCfgMapper()));
            jdbcCall.addDeclaredParameter(new SqlOutParameter("AVCH_MESSAGE", OracleTypes.VARCHAR));

            Map<String, Object> result = jdbcCall.execute();
            String message = (String) result.get("AVCH_MESSAGE");

            if (message != null) {
                throw new RepositoryException(message);
            }

            List<FrsHssRequestCfg> list = (List<FrsHssRequestCfg>) result.get("ACUR_CONFIG_LIST");

            if (list == null || list.isEmpty()) {
                list = new ArrayList<FrsHssRequestCfg>();
            }

            logger.info("Parametros configurados: " + list.size());

            return list;
        } catch (Exception e) {
            throw new RepositoryException(e);
        }
    }

    public void update(FrsHssRequestCfg cfg) throws RepositoryException {
        try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(ds);
            jdbcCall.withSchemaName(schema);
            jdbcCall.withCatalogName(mainPackage);
            jdbcCall.withProcedureName("SP_RS_CD_UPD_REQUEST_CFG");

            jdbcCall.addDeclaredParameter(new SqlParameter("ATO_REQUEST", OracleTypes.STRUCT, cfg.getSQLTypeName()));
            jdbcCall.addDeclaredParameter(new SqlOutParameter("AVCH_MESSAGE", OracleTypes.VARCHAR));

            SqlParameterSource sqlParams = new MapSqlParameterSource()
                    .addValue("ATO_REQUEST", cfg);

            Map<String, Object> result = jdbcCall.execute(sqlParams);
            String message = (String) result.get("AVCH_MESSAGE");

            if (message != null) {
                throw new RepositoryException(message);
            }
        } catch (Exception e) {
            throw new RepositoryException(e);
        }
    }


}
