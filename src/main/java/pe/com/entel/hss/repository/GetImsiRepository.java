package pe.com.entel.hss.repository;

import oracle.jdbc.OracleTypes;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import pe.com.entel.hss.domain.Subscriber;
import pe.com.entel.hss.exception.RepositoryException;
import pe.com.entel.hss.util.TelnetClientHandler;
import org.springframework.web.client.RestTemplate;

import javax.sql.DataSource;
import java.util.Map;

/**
 * @version 1.0, 04/02/2019
 * @autor wmarcelo
 */
@Repository
public class GetImsiRepository {

    private static Logger logger = Logger.getLogger(GetImsiRepository.class);

    @Autowired
    @Qualifier("pbscsDataSource")
    private DataSource ds;

    @Value("${hss.bd.pbscs.schema}")
    private String schema;

    @Value("${hss.bd.pbscs.package}")
    private String mainPackage;

    RestTemplate restTemplate = new RestTemplate();

    public String getByImei(String imei) throws RepositoryException {
        logger.debug("imei inside: " + imei);

        /*try {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(ds);
            jdbcCall.withSchemaName(schema);
            jdbcCall.withCatalogName(mainPackage);
            jdbcCall.withProcedureName("SP_RS_CD_GETIMSI");

            jdbcCall.addDeclaredParameter(new SqlParameter("AVCH_IMEI", OracleTypes.VARCHAR));
            jdbcCall.addDeclaredParameter(new SqlOutParameter("AVCH_IMSI", OracleTypes.VARCHAR));
            jdbcCall.addDeclaredParameter(new SqlOutParameter("AVCH_MESSAGE", OracleTypes.VARCHAR));

            SqlParameterSource sqlParams = new MapSqlParameterSource()
                    .addValue("AVCH_IMEI", imei);

            Map<String, Object> result = jdbcCall.execute(sqlParams);
            String message = (String) result.get("AVCH_MESSAGE");

            logger.debug("message: " + message);

            if (message != null) {
                throw new RepositoryException(message);
            }

            Object imsi = result.get("AVCH_IMSI");

            logger.debug("imsi inside: " + imsi);

            if (imsi == null) {
                return null;
            }

            return String.valueOf(imsi);
        } catch (Exception e) {
            throw new RepositoryException(e);
        }*/
        return null;
    }

    public String getImsiFromDMS(String ROOT_URI, String imei) throws RepositoryException {
        try{
            Subscriber subscriber = restTemplate.getForObject(ROOT_URI+"?imei="+imei, Subscriber.class);
            return subscriber.getImsi();
        }catch (Exception e){
            logger.error("No se pudo obtener el IMSI para el IMEI "+imei+": "+ e.getMessage());
        }
        return null;
    }
}
