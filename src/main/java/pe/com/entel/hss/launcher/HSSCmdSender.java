package pe.com.entel.hss.launcher;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import pe.com.entel.hss.service.*;

/**
 * @version 1.0, 06/01/2018
 * @autor jsegovia
 */
@Component
public class HSSCmdSender {

    static Logger logger = Logger.getLogger(HSSCmdSender.class);

    public static void main(String args[]) {
        logger.info(">>>> Inicio HSS Command Manager <<<<");
        ClassPathXmlApplicationContext ctx = null;
        try {
            ctx = new ClassPathXmlApplicationContext("spring-context.xml");
            ctx.registerShutdownHook();

            HSSInitialConfig configManager = ctx.getBean(HSSInitialConfig.class);
            logger.debug("configManager: " + configManager);
            configManager.loadParameter();

            HSSMoveBlackListToRequest moveToRequest = ctx.getBean(HSSMoveBlackListToRequest.class);
            logger.debug("moveToRequest: " + moveToRequest);
            moveToRequest.exec();

            HSSCmdManagerService mainService = ctx.getBean(HSSCmdManagerService.class);
            logger.debug("mainService: " + mainService);
            mainService.sendParalelCmds();

            HSSSendMail sendMailService = ctx.getBean(HSSSendMail.class);
            logger.debug("sendMailService: " + sendMailService);
            sendMailService.exec();

            HSSMoveToHistory moveToHistory = ctx.getBean(HSSMoveToHistory.class);
            logger.debug("moveToHistory: " + moveToHistory);
            moveToHistory.exec();

        } catch (Exception e) {
            logger.error(e);
        } finally {
            if (ctx != null) {
                ctx.close();
            }
        }
        logger.info(">>>> FIN HSS Command Manager <<<<");
    }

}
